/**
 * @file
 */

(function ($, Drupal) {

    'use strict';

    /**
     * Registers behaviours related to Bynder upload widget.
     */
    Drupal.behaviors.bynderUpload = {
      attach(context) {
        $(once('bynder-asset-upload', '#edit-submit', context)).click();
      }
    };

}(jQuery, Drupal));
