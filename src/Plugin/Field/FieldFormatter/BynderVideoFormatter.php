<?php

namespace Drupal\bynder\Plugin\Field\FieldFormatter;

use Drupal\bynder\Plugin\media\Source\Bynder;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'Bynder Video' formatter.
 *
 * @FieldFormatter(
 *   id = "bynder_video",
 *   label = @Translation("Bynder (Video)"),
 *   field_types = {"string", "string_long"}
 * )
 */
class BynderVideoFormatter extends BynderFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'controls' => TRUE,
      'autoplay' => FALSE,
      'loop' => FALSE,
      'muted' => FALSE,
      'width' => 640,
      'height' => 480,
    ] + parent::defaultSettings();
  }
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['controls'] = [
      '#title' => $this->t('Show playback controls'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('controls'),
    ];
    $elements['autoplay'] = [
      '#title' => $this->t('Autoplay'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('autoplay'),
    ];
    $elements['loop'] = [
      '#title' => $this->t('Loop'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('loop'),
    ];
    $elements['muted'] = [
      '#title' => $this->t('Muted'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('muted'),
    ];
    $elements['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $this->getSetting('width'),
      '#size' => 5,
      '#maxlength' => 5,
      '#field_suffix' => $this->t('pixels'),
      '#min' => 0,
      '#required' => TRUE,
    ];
    $elements['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->getSetting('height'),
      '#size' => 5,
      '#maxlength' => 5,
      '#field_suffix' => $this->t('pixels'),
      '#min' => 0,
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Controls: %controls', ['%controls' => $this->getSetting('controls') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Autoplay: %autoplay', ['%autoplay' => $this->getSetting('autoplay') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Loop: %loop', ['%loop' => $this->getSetting('loop') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Muted: %muted', ['%muted' => $this->getSetting('muted') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Size: %width x %height pixels', [
      '%width' => $this->getSetting('width'),
      '%height' => $this->getSetting('height'),
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    $is_entityreference = $this->fieldDefinition->getType() == 'entity_reference';

    foreach ($items as $delta => $item) {

      /** @var \Drupal\media\MediaInterface $media */
      $media = $is_entityreference ? $item->entity : $items->getEntity();
      if (!$media) {
        continue;
      }
      $source_plugin = $media->getSource();
      if ($source_plugin instanceof Bynder && ($video_preview_urls = $source_plugin->getMetadata($media, 'videoPreviewURLs'))) {
        $attributes = new Attribute();
        $attributes->setAttribute('controls', $this->getSetting('controls'))
          ->setAttribute('autoplay', $this->getSetting('autoplay'))
          ->setAttribute('loop', $this->getSetting('loop'))
          ->setAttribute('muted', $this->getSetting('muted'))
          ->setAttribute('width', $this->getSetting('width'))
          ->setAttribute('height', $this->getSetting('height'));
        $source_attributes = [];
        foreach ($video_preview_urls as $video_url) {
          $source_attribute = new Attribute();

          // If the url is relative, make it external using the account domain.
          if (!preg_match('/^https?:/', $video_url)) {
            $bynderSettings = $this->configFactory->get('bynder.settings');
            $accountDomain = $bynderSettings->get('account_domain');
            $accountDomain = rtrim($accountDomain, '/');
            $completeUrl = "https://$accountDomain$video_url";
            if (UrlHelper::isValid($completeUrl)) {
              $video_url = $completeUrl;
            }
          }

          $source_attribute->setAttribute('src', $video_url);

          // Try to get the extension from metadata.
          // If unable to, get if from the filename.
          // Default to mp4.
          $extensionMetaData = $source_plugin->getMetadata($media, 'extension');
          $extensionType = 'mp4';
          if (!empty($extensionMetaData) && !empty($extensionMetaDataValue = reset($extensionMetaData))) {
            $extensionType = $extensionMetaDataValue;
          }
          else {
            $path_info = pathinfo($video_url);
            if ($path_info['extension']) {
              $extensionType = $path_info['extension'];
            }
          }

          $source_attribute->setAttribute('type', "video/$extensionType");
          $source_attributes[] = $source_attribute;
        }
        $elements[] = [
          '#theme' => 'bynder_video',
          '#attributes' => $attributes,
          '#source_attributes' => $source_attributes,
        ];
      }
    }

    return $elements;
  }

}
