<?php

namespace Drupal\Tests\bynder\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests the Configuration form for bynder API.
 *
 * @group bynder
 */
class ConfigurationFormTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['bynder', 'help', 'bynder_test_module'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->drupalCreateUser([
      'administer bynder configuration',
      'access administration pages',
    ]));

    // Set dummy initial values to prevent derivatives batch reload from
    // triggering on the initial save.
    $this->config('bynder.settings')
      ->set('permanent_token', 'token')
      ->set('client_id', 'id')
      ->set('client_secret', 'secret')
      ->set('account_domain', 'account_domain')
      ->save();
  }

  /**
   * Tests the configuration form.
   */
  public function testConfigurationForm() {
    $old_hash = \Drupal::state()->get('bynder_config_hash');
    $this->assertNotEmpty($old_hash, 'Config hash is calculated.');
    \Drupal::state()->set('bynder.bynder_test_brands', 'valid data');
    $this->drupalGet('admin/config/services');
    $this->assertSession()->pageTextContains('Bynder');
    $this->assertSession()->pageTextContains('Configure integration with Bynder');

    $this->clickLink('Bynder');
    $this->assertSession()->pageTextContains('Usage restriction metadata');
    $this->assertSession()->pageTextContains('To set usage restriction metaproperty provide valid credentials first.');

    $this->assertSession()->pageTextContains('Permanent token');
    $this->assertSession()->responseContains('The permanent token. Create one on your-bynder-domain/pysettings.');
    $this->getSession()->getPage()->fillField('credentials[permanent_token]', 'permanent_token_form');

    $this->assertSession()->pageTextContains('Client ID');
    $this->assertSession()->responseContains('Certain features like upload also require an OAuth application with client ID and secret. Create one on your-bynder-domain/pysettings.');
    $this->getSession()->getPage()->fillField('credentials[client_id]', 'client_id_form');

    $this->assertSession()->pageTextContains('client secret');
    $this->getSession()->getPage()->fillField('credentials[client_secret]', 'client_secret_form');

    $this->assertSession()->pageTextContains('Account domain');
    $this->assertSession()->responseContains('Provide your Bynder account domain, typically [name].bynder.com.');
    $this->getSession()->getPage()->fillField('credentials[account_domain]', 'test.bynder.com');

    $this->assertSession()->pageTextContains('Debug');
    $this->assertSession()->pageTextContains('Check this setting if you want to have more verbose log messages.');
    $this->getSession()->getPage()->fillField('debug', TRUE);

    $this->assertSession()->pageTextContains('Test connection before saving');
    $this->assertSession()->pageTextContains("Uncheck to allow saving credentials even if connection to Bynder can't be established.");
    $this->assertSession()->checkboxChecked('test_connection');

    $this->getSession()->getPage()->pressButton('Save configuration');
    $this->assertNotEquals($old_hash, \Drupal::state()->get('bynder_config_hash'), 'Config hash was updated after the configuration was changed.');

    $metaproperties = [
      'property1' => [
        'label' => 'Property 1',
        'options' => [
          0 => [
            'label' => 'Option 1',
            'id' => 'option1',
          ],
          1 => [
            'label' => 'Option 2',
            'id' => 'option2',
          ],
          2 => [
            'label' => 'Option 3',
            'id' => 'option3',
          ],
        ],
      ],
      'property2' => [
        'label' => 'Property 2',
        'options' => [],
      ],
    ];
    \Drupal::state()->set('bynder.bynder_test_metaproperties', $metaproperties);

    $this->drupalGet('admin/config/services/bynder');
    $this->assertSession()->fieldValueEquals('credentials[permanent_token]', 'permanent_token_form');
    $this->assertSession()->fieldValueEquals('credentials[client_id]', 'client_id_form');
    $this->assertSession()->fieldValueEquals('credentials[client_secret]', 'client_secret_form');
    $this->assertSession()->fieldValueEquals('credentials[account_domain]', 'test.bynder.com');
    $this->assertSession()->fieldValueEquals('debug', TRUE);
    $this->assertSession()->selectExists('usage_metaproperty');
    $this->assertSession()->optionExists('usage_metaproperty', 'property1');
    $this->assertSession()->optionNotExists('usage_metaproperty', 'property2');
    $this->assertSession()->pageTextNotContains('Royalty free restriction level');
    $this->assertSession()->pageTextNotContains('Web license restriction level');
    $this->assertSession()->pageTextNotContains('Print license restriction level');

    $this->getSession()->getPage()->selectFieldOption('usage_metaproperty', 'property1');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->selectFieldOption('restrictions[royalty_free]', 'option1');
    $this->getSession()->getPage()->selectFieldOption('restrictions[web_license]', 'option2');
    $this->getSession()->getPage()->selectFieldOption('restrictions[print_license]', 'option3');

    $this->getSession()->getPage()->pressButton('Save configuration');

    $this->assertSession()->pageTextContains('Select metaproperty which is responsible for usage restriction. This is used to limit what assets can be used. If the information is not provided we assume royalty free.');
    $this->assertSession()->pageTextContains('Select metaproperty option for assets that can be used everywhere.');
    $this->assertSession()->pageTextContains('Select metaproperty option for the assets that may be used only online.');
    $this->assertSession()->pageTextContains('Select metaproperty option for the assets that may be used only for print.');

    $this->assertSession()->elementExists('xpath', '//select[@name="restrictions[royalty_free]"]//option[@selected="selected" and @value="option1"]');
    $this->assertSession()->elementExists('xpath', '//select[@name="restrictions[web_license]"]//option[@selected="selected" and @value="option2"]');
    $this->assertSession()->elementExists('xpath', '//select[@name="restrictions[print_license]"]//option[@selected="selected" and @value="option3"]');

    $this->getSession()->getPage()->selectFieldOption('usage_metaproperty', 'none');
    $this->assertSession()->assertWaitOnAjaxRequest();

    $this->assertSession()->pageTextNotContains('Royalty free restriction level');
    $this->assertSession()->pageTextNotContains('Web license restriction level');
    $this->assertSession()->pageTextNotContains('Print license restriction level');

    \Drupal::state()->set('bynder.bynder_test_brands', NULL);
    $this->drupalGet('admin/config/services/bynder');
    $this->getSession()->getPage()->pressButton('Save configuration');
    $this->assertSession()->pageTextContains('Could not establish connection with Bynder. Check your credentials or contact support.');
    $this->drupalGet('admin/config/services/bynder');
    $this->getSession()->getPage()->fillField('test_connection', FALSE);
    $this->getSession()->getPage()->pressButton('Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->pageTextNotContains('Could not establish connection with Bynder. Check your credentials or contact support.');

    $this->drupalGet('admin/config/services/bynder');
    $this->getSession()->getPage()->pressButton('Test connection');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->pageTextContains('Could not establish connection with Bynder. Check your credentials or contact support.');
    \Drupal::state()->set('bynder.bynder_test_brands', 'valid data');
    $this->getSession()->getPage()->pressButton('Test connection');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->pageTextContains('The API connection was established successfully.');
  }

}
